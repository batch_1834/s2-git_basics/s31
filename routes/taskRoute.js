const express = require("express");
// The "taskController" allow us to use the function defined inside it.
const taskController = require("../controllers/taskController");

// Allows access to HTTp Method middlewares that makes it easier to create routes for our applicaation.
const router = express.Router();

// Route to all tasks
router.get("/", (req, res)=>{

	taskController.getAllTasks().then(resultFromController => res.send(
		resultFromController));
})


// Route Create a Task
// localhost:3001/tsak - It is the same with the localhost:3001/task/
router.post("/", (req, res) => {
	// The "createtask" function needs data from the request body, so we need it to supply in the taskController.createtask(argument).
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Route to delete a task
// colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the url.
// "/id:" is a wildcard where you can put the objectID as the value.
// Ex: localhost:3001/tasks/:id localhost:3000/tasks/123456
router.delete("/:id",(req, res) =>{
	// If information will be coming from the URL, the data can be accessed from the request "params" property
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Route to update a task

router.patch("/:id", (req, res) =>{
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})


router.get("/task/:id",(req, res)=>{
	taskController.findOne().then(resultFromController => res.send(resultFromController));
})

router.post("/tasks/:id", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})



// Use "module.exports" to export the router object to be use in the server
module.exports = router;



